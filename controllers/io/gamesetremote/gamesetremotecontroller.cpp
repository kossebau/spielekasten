/*
    This file is part of the Spiele Kasten Framework, made within the KDE community.

    Copyright 2011 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#include "gamesetremotecontroller.h"

// KDE Games
#include <KStandardGameAction>
// Kasten core
#include <Kasten2/DocumentSyncManager>
#include <Kasten2/AbstractDocument>
// KDE
#include <KActionCollection>
#include <KAction>
#include <KXMLGUIClient>

#include <QDebug>
namespace Kasten2
{

GameSetRemoteController::GameSetRemoteController( DocumentSyncManager* syncManager,
                                                  KXMLGUIClient* guiClient )
  : mSyncManager( syncManager )
{
    KActionCollection* const actionCollection = guiClient->actionCollection();

    mSaveAsAction = KStandardGameAction::saveAs( this, SLOT(saveAs()), actionCollection );

    setTargetModel( 0 );
}

void GameSetRemoteController::setTargetModel( AbstractModel* model )
{
    mDocument = model ? model->findBaseModel<AbstractDocument*>() : 0;
qDebug()<<model<<mDocument;
    const bool canBeSaved = mDocument ?
                                ( mDocument->synchronizer() != 0 ||
                                  mSyncManager->hasSynchronizerForLocal(mDocument->mimeType()) ) :
                                false;
if(mDocument)
qDebug() << "GameSetRemoteController::setTargetModel" << model << (mDocument->synchronizer() != 0) << mSyncManager->hasSynchronizerForLocal(mDocument->mimeType()) << mDocument->mimeType();
    mSaveAsAction->setEnabled( canBeSaved );
}

void GameSetRemoteController::saveAs()
{
    mSyncManager->setSynchronizer( mDocument );
}

GameSetRemoteController::~GameSetRemoteController()
{
}

}
