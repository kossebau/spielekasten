/*
    This file is part of the Spiele Kasten Framework, made within the KDE community.

    Copyright 2011 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#include "gameturnstatusbarcontroller.h"

// lib
#include <gameturnable.h>
// Kasten ui
#include <Kasten2/StatusBar>
// Kasten core
#include <Kasten2/AbstractModel>
// KDE
#include <KLocale>
// Qt
#include <QtGui/QLabel>
#include <QtGui/QFontMetrics>

#include <KDebug>

namespace Kasten2
{

GameTurnStatusBarController::GameTurnStatusBarController( StatusBar* statusBar )
  : AbstractXmlGuiController()
  , mStatusBar( statusBar )
  , mModel( 0 )
  , mGameTurnable( 0 )
{
    mGameTurnStatusLabel = new QLabel( statusBar );
    statusBar->addWidget( mGameTurnStatusLabel );

    fixWidths();

    setTargetModel( 0 );
}

static QString turnStatusLabel( int gameTurnStatus )
{
    return
        gameTurnStatus == GameNotStarted ? i18n("Not started") :
        gameTurnStatus == GameOver ?       i18n("GAME OVER") :
        gameTurnStatus == GameMyTurn ?     i18n("Your turn.") :
        gameTurnStatus == GameOtherTurn ?  i18n("Computer") : //? i18n("Opponent")
        /*else*/ i18nc("@info:status turn status not available",
                       "-");
}

void GameTurnStatusBarController::fixWidths()
{
    const QFontMetrics metrics = mStatusBar->fontMetrics();

    int largestTurnStatusWidth = 0;
    for( int turnStatus=GameNotStarted; turnStatus<=GameOver; ++turnStatus )
    {
        const int turnStatusWidth = metrics.boundingRect( turnStatusLabel(turnStatus) ).width();
        if( largestTurnStatusWidth < turnStatusWidth )
            largestTurnStatusWidth = turnStatusWidth;
    }

    mGameTurnStatusLabel->setFixedWidth( largestTurnStatusWidth );
}

void GameTurnStatusBarController::setTargetModel( AbstractModel* model )
{
    if( mModel ) mModel->disconnect( this );

    mModel = model ? model->findBaseModelWithInterface<If::GameTurnable*>() : 0;
    mGameTurnable = mModel ? qobject_cast<If::GameTurnable*>( mModel ) : 0;

    const bool hasView = ( mGameTurnable != 0 );
    if( hasView )
    {
        connect( mModel, SIGNAL(gameTurnStatusChanged(int)),
                 SLOT(onGameTurnStatusChanged(int)) );

        onGameTurnStatusChanged( mGameTurnable->gameTurnStatus() );
    }
    else
        mGameTurnStatusLabel->setText( turnStatusLabel(GameTurnStatusUnknown) );

    mGameTurnStatusLabel->setEnabled( hasView );
}


void GameTurnStatusBarController::onGameTurnStatusChanged( int gameTurnStatus )
{
    mGameTurnStatusLabel->setText( turnStatusLabel(gameTurnStatus) );
}

}
