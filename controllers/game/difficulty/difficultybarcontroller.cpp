/*
    This file is part of the Spiele Kasten Framework, made within the KDE community.

    Copyright 2011 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#include "difficultybarcontroller.h"

// lib
#include <gamedifficultable.h>
// Kasten Gui
#include <Kasten2/StatusBar>
// Kasten Core
#include <Kasten2/AbstractModel>
// KDE
#include <KLocale>
#include <KComboBox>
#include <KIcon>


namespace Kasten2
{

DifficultyBarController::DifficultyBarController( StatusBar* statusBar )
    : AbstractXmlGuiController()
    , mModel( 0 )
    , mGameDifficultable( 0 )
{
    mDifficultyComboBox = new KComboBox( statusBar );
    statusBar->addWidget( mDifficultyComboBox );

    mDifficultyComboBox->setToolTip( i18n("Set the difficulty level") );
    mDifficultyComboBox->setWhatsThis( i18n("Set the difficulty level of the game.") );
    connect( mDifficultyComboBox, SIGNAL(activated(int)), SLOT(setDifficulty(int)) );

    setTargetModel( 0 );
}


void DifficultyBarController::setTargetModel( AbstractModel* model )
{
    if( mModel ) mModel->disconnect( this );

    mModel = model ? model->findBaseModelWithInterface<If::GameDifficultable*>() : 0;
    mGameDifficultable = mModel ? qobject_cast<If::GameDifficultable*>( mModel ) : 0;

    mDifficultyComboBox->clear();

    if( mGameDifficultable )
    {
        mGameDifficulties = mGameDifficultable->possibleGameDifficulties();
        const KIcon icon = KIcon( QLatin1String("games-difficult") );

        foreach( GameDifficulty difficulty, mGameDifficulties )
            mDifficultyComboBox->addItem( icon, gameDifficultyToLable(difficulty) );

        onGameDifficultyChanged( mGameDifficultable->gameDifficulty() );

        connect( mModel, SIGNAL(gameDifficultyChanged(int)),
                 SLOT(onGameDifficultyChanged(int)) );
    }

    const bool hasView = ( mGameDifficultable != 0 );
    mDifficultyComboBox->setEnabled( hasView );
}

void DifficultyBarController::setDifficulty( int difficultyIndex )
{
    mGameDifficultable->setGameDifficulty( mGameDifficulties.at(difficultyIndex) );
}

void DifficultyBarController::onGameDifficultyChanged( int difficulty )
{
    const int difficultyIndex = mGameDifficulties.indexOf( GameDifficulty(difficulty) );
    mDifficultyComboBox->setCurrentIndex( difficultyIndex );
}

}
