/*
    This file is part of the Spiele Kasten Framework, made within the KDE community.

    Copyright 2011 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#include "hintcontroller.h"

// Spiele Kasten Gui
#include <hintable.h>
// KDE Games
#include <KStandardGameAction>
// Kasten core
#include <Kasten2/AbstractModel>
// KDE
#include <KActionCollection>
#include <KXMLGUIClient>
#include <KAction>


namespace Kasten2
{

HintController::HintController( KXMLGUIClient* guiClient )
{
    KActionCollection* const actionCollection = guiClient->actionCollection();
    mHintAction =
        KStandardGameAction::hint( this, SLOT(onHintActionTriggered()), actionCollection );

    setTargetModel( 0 );
}

void HintController::setTargetModel( AbstractModel* model )
{
    AbstractModel* baseModel =
        model ? model->findBaseModelWithInterface<If::Hintable*>() : 0;
    mHintable = baseModel ? qobject_cast<If::Hintable*>( baseModel ) : 0;

    const bool hasView = ( mHintable != 0 );
    mHintAction->setEnabled( hasView );
}

void HintController::onHintActionTriggered()
{
    mHintable->hint();
}

HintController::~HintController() {}

}
