/*
    This file is part of the Spiele Kasten Framework, made within the KDE community.

    Copyright 2011 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#include "democontroller.h"

// Spiele Kasten Gui
#include <demoable.h>
// KDE Games
#include <KStandardGameAction>
// Kasten core
#include <Kasten2/AbstractModel>
// KDE
#include <KActionCollection>
#include <KXMLGUIClient>
#include <KToggleAction>


namespace Kasten2
{

DemoController::DemoController( KXMLGUIClient* guiClient )
    : AbstractXmlGuiController()
    , mModel( 0 )
    , mDemoable( 0 )
{
    KActionCollection* const actionCollection = guiClient->actionCollection();
    mDemoAction =
        KStandardGameAction::demo( this, SLOT(onDemoActionTriggered(bool)), actionCollection );

    setTargetModel( 0 );
}

void DemoController::setTargetModel( AbstractModel* model )
{
    if( mModel ) mModel->disconnect( this );
    mModel =
        model ? model->findBaseModelWithInterface<If::Demoable*>() : 0;
    mDemoable = mModel ? qobject_cast<If::Demoable*>( mModel ) : 0;

    if( mDemoable )
    {
        onDemoModusChanged( mDemoable->demoModus() );

        connect( mModel, SIGNAL(demoModusChanged(bool)),
                 SLOT(onDemoModusChanged(bool)) );
    }
    else
        mDemoAction->setChecked( false );

    const bool hasView = ( mDemoable != 0 );
    mDemoAction->setEnabled( hasView );
}

void DemoController::onDemoActionTriggered( bool triggered )
{
    mDemoable->setDemoModus( triggered );
}

void DemoController::onDemoModusChanged( bool modus )
{
    mDemoAction->setChecked( modus );
}

DemoController::~DemoController() {}

}
