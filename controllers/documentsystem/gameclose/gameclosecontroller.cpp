/*
    This file is part of the Spiele Kasten Framework, made within the KDE community.

    Copyright 2011 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#include "gameclosecontroller.h"

// KDE Games
#include <KStandardGameAction>
// Kasten Gui
#include <Kasten2/AbstractDocumentStrategy>
#include <Kasten2/AbstractDocument>
// KDE
#include <KActionCollection>
#include <KAction>
#include <KStandardAction>
#include <KXMLGUIClient>
#include <KLocale>
#include <KIcon>


namespace Kasten2
{

GameCloseController::GameCloseController( AbstractDocumentStrategy* documentStrategy,
                                          KXMLGUIClient* guiClient,
                                          bool supportMultiple )
  : AbstractXmlGuiController()
  , mDocumentStrategy( documentStrategy )
  , mDocument( 0 )
{
    KActionCollection* const actionCollection = guiClient->actionCollection();

    // TODO: add close action for games
//     mCloseAction  = KStandardGameAction::close( this, SLOT(close()),  actionCollection );
//     mCloseAction->setEnabled( false );

    if( false/*supportMultiple*/ )
    {
        mCloseAllAction = actionCollection->addAction( QLatin1String("file_close_all") );
        mCloseAllAction->setText( i18nc("@title:menu","Close All") );
        mCloseAllAction->setIcon( KIcon( QLatin1String("window-close") ) );
        mCloseAllAction->setEnabled( false );
        connect( mCloseAllAction, SIGNAL(triggered(bool)), SLOT(closeAll()) );

        mCloseAllOtherAction = actionCollection->addAction( QLatin1String("file_close_all_other") );
        mCloseAllOtherAction->setText( i18nc("@title:menu","Close All Other") );
        mCloseAllOtherAction->setIcon( KIcon( QLatin1String("window-close") ) );
        mCloseAllOtherAction->setEnabled( false );
        connect( mCloseAllOtherAction, SIGNAL(triggered(bool)), SLOT(closeAllOther()) );

        connect( mDocumentStrategy, SIGNAL(added(QList<Kasten2::AbstractDocument*>)),
                SLOT(onDocumentsChanged()) );
        connect( mDocumentStrategy, SIGNAL(closing(QList<Kasten2::AbstractDocument*>)),
                SLOT(onDocumentsChanged()) );
    }
}

void GameCloseController::setTargetModel( AbstractModel* model )
{
    return;
    mDocument = model ? model->findBaseModel<AbstractDocument*>() : 0;
    const bool hasDocument = ( mDocument != 0 );

    mCloseAction->setEnabled( hasDocument );
}


void GameCloseController::close()
{
    if( mDocumentStrategy->canClose(mDocument) )
        mDocumentStrategy->closeDocument( mDocument );
}

void GameCloseController::closeAll()
{
    if( mDocumentStrategy->canCloseAll() )
        mDocumentStrategy->closeAll();
}

void GameCloseController::closeAllOther()
{
    if( mDocumentStrategy->canCloseAllOther(mDocument) )
        mDocumentStrategy->closeAllOther( mDocument );
}

void GameCloseController::onDocumentsChanged()
{
    const QList<AbstractDocument*> documents = mDocumentStrategy->documents();

    const bool hasDocuments = ! documents.isEmpty();
    // TODO: there could be just one, but not set for this tool?
    const bool hasOtherDocuments = ( documents.size() > 1 );

    mCloseAllAction->setEnabled( hasDocuments );
    mCloseAllOtherAction->setEnabled( hasOtherDocuments );
}

GameCloseController::~GameCloseController()
{
}

}
