/*
    This file is part of the Spiele Kasten Framework, made within the KDE community.

    Copyright 2011 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#include "gamecreatorcontroller.h"

// KDE Games
#include <KStandardGameAction>
// Kasten Gui
#include <Kasten2/AbstractDocumentStrategy>
// KDE
#include <KActionCollection>
#include <KXMLGUIClient>

namespace Kasten2
{

GameCreatorController::GameCreatorController( AbstractDocumentStrategy* documentStrategy,
                                              KXMLGUIClient* guiClient )
  : AbstractXmlGuiController(),
    mDocumentStrategy( documentStrategy )
{
    KActionCollection* const actionCollection = guiClient->actionCollection();
    KStandardGameAction::gameNew( this, SLOT(onNewActionTriggered()), actionCollection );
}
// TODO: general controller should be able to test if clipboard data can/should be used
// TODO: text should be able to adapt to context. "Start a new game" vs. "Create a new document."

void GameCreatorController::setTargetModel( AbstractModel* model )
{
Q_UNUSED( model )
}

void GameCreatorController::onNewActionTriggered()
{
    // TODO: pass context (current ViewArea), so known where new view should be placed
    mDocumentStrategy->createNew();
}

GameCreatorController::~GameCreatorController() {}

}
