/*
    This file is part of the Spiele Kasten Framework, made within the KDE community.

    Copyright 2011 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#include "gameloadercontroller.h"

// KDE Games
#include <KStandardGameAction>
// Kasten core
#include <Kasten2/AbstractDocumentStrategy>
// KDE
#include <KUrl>
#include <KFileDialog>
#include <KRecentFilesAction>
#include <KActionCollection>
#include <KXMLGUIClient>
#include <KConfigGroup>
#include <KGlobal>


namespace Kasten2
{

static const char CreatorConfigGroupId[] = "Recent Files";


GameLoaderController::GameLoaderController( AbstractDocumentStrategy* documentStrategy,
                                            KXMLGUIClient* guiClient )
  : AbstractXmlGuiController()
  , mDocumentStrategy( documentStrategy )
{
    KActionCollection* const actionCollection = guiClient->actionCollection();

    KStandardGameAction::load( this, SLOT(load()), actionCollection );
    mOpenRecentAction =
        KStandardGameAction::loadRecent( this, SLOT(loadRecent(KUrl)), actionCollection );

    KConfigGroup configGroup( KGlobal::config(), CreatorConfigGroupId );
    mOpenRecentAction->loadEntries( configGroup );

    connect( mDocumentStrategy, SIGNAL(urlUsed(KUrl)), SLOT(onUrlUsed(KUrl)) );
}


void GameLoaderController::setTargetModel( AbstractModel* model )
{
Q_UNUSED( model )
}

void GameLoaderController::load()
{
    const QString filterString = mDocumentStrategy->supportedRemoteTypes().join( QLatin1String(" ") );

    const KUrl url =
        KFileDialog::getOpenUrl( KUrl()/*mWorkingUrl.url()*/, filterString, /*mWidget*/0 );

    if( ! url.isEmpty() )
        mDocumentStrategy->load( url );
}

void GameLoaderController::loadRecent( const KUrl& url )
{
    mDocumentStrategy->load( url );
}

void GameLoaderController::onUrlUsed( const KUrl& url )
{
    mOpenRecentAction->addUrl( url );
}

GameLoaderController::~GameLoaderController()
{
    KConfigGroup configGroup( KGlobal::config(), CreatorConfigGroupId );
    mOpenRecentAction->saveEntries( configGroup );
}

}
