/*
    This file is part of the Spiele Kasten Framework, made within the KDE community.

    Copyright 2011 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef KASTEN_IF_GAMEDIFFICULTABLE_H
#define KASTEN_IF_GAMEDIFFICULTABLE_H

// library
#include <gamedifficulty.h>
// Qt
#include <QtCore/QtPlugin>

template<typename C> class QVector;


namespace Kasten2
{
namespace If
{

class GameDifficultable
{
  public:
    virtual ~GameDifficultable();

  public: // set/action
    virtual QVector<GameDifficulty> possibleGameDifficulties() const = 0;
    virtual GameDifficulty gameDifficulty() const = 0;

    virtual void setGameDifficulty( GameDifficulty difficulty ) = 0;

  public: // signals
    virtual void gameDifficultyChanged( int difficulty ) = 0;
};

inline GameDifficultable::~GameDifficultable() {}

}
}

Q_DECLARE_INTERFACE( Kasten2::If::GameDifficultable, "org.kde.kasten2.if.gamedifficultable/1.0" )

#endif
