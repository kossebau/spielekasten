/*
    This file is part of the Spiele Kasten Framework, made within the KDE community.

    Copyright 2011 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#include "gameturnstatus.h"

// KDE
#include <KLocale>

namespace Kasten2
{

// QString gameDifficultyToLable( GameDifficulty level )
// {
//     QString result;
// 
//     switch( level )
//     {
//         case GameRidiculouslyEasy:
//             return i18nc("Game difficulty level 1 out of 8",
//                          "Ridiculously Easy");
//         case GameVeryEasy:
//             return i18nc("Game difficulty level 2 out of 8",
//                          "Very Easy");
//         case GameEasy:
//             return i18nc("Game difficulty level 3 out of 8",
//                          "Easy");
//         case GameMedium:
//             return i18nc("Game difficulty level 4 out of 8",
//                          "Medium");
//         case GameHard:
//             return i18nc("Game difficulty level 5 out of 8",
//                          "Hard");
//         case GameVeryHard:
//             return i18nc("Game difficulty level 6 out of 8",
//                          "Very Hard");
//         case GameExtremelyHard:
//             return i18nc("Game difficulty level 7 out of 8",
//                          "Extremely Hard");
//         case GameImpossible:
//             return i18nc("Game difficulty level 8 out of 8",
//                          "Impossible");
//         case GameCustomDifficulty:
//         case GameConfigurable:
//         case GameNoDifficulty:
//             // Do nothing
//             break;
//     }
//     return result;
// }

}
